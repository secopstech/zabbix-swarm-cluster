# Zabbix deployment on Docker Swarm


Zabbix server deployment for docker swarm with secrets support. It deploys four seperated services named zabbix-db, 
zabbix-server, zabbix-ui, zabbix-smtp.

- zabbix-db is mysql backend for the zabbix-server
- zabbix-server is the zabbix engine 
- zabbix-ui is nginx frontend for the zabbix-server
- zabbix-smtp is a mini qmail server that grants relay from overlay network (zabbix-net) which
you can configure the zabbix server smtp settings to send notification mails w/o authenticatiom.
It can be resolved as ***zabbix-smtp*** in the zabbix-net overlay network and only accepts mails 
from zabbix cluster overlay network to outbound.

While zabbix-server and zabbix-ui services use the zabbix images forked from the official zabbix repo
and include some minor changes for secrets support, zabbix-db service uses secopstech/alpine-mariadb 
image also includes some tweaks such as auto generation of mysql.cnf based on the container's memory limit 
and zabbix-smtp service uses secopstec/mini-qmail:0.2 image contains a basic qmail installation.

**Important Note:** For the data persistency zabbix-db and zabbix-server services should be scheduled to specific node
which means you need to change ***constraints*** value for these services with your node's hostname in the compose file.
It creates a named volume for each services. Also you may want to change memory limits in the ***resource*** sections.

## Deploy Docker Stack


First, you need to create two secrets for mysql root password and mysql zabbix user password like below:

```commandline

# date |base64 |docker secret create mysql_root_passwd -
# date |base64 |docker secret create mysql_zabbix_passwd -

```
Then make necessary changes in the docker-compose.yml and run:

```commandline
# docker stack deploy -c zabbix-deploy.yml zabbix
```